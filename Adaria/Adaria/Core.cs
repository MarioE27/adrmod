using System;
using System.Collections.Generic;
using System.Reflection;
using System.IO;
using System.Net;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using Terraria;

using AdrenicLib;

namespace ADRMod
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public partial class Core : Main
    {
        public static Adrenic Adrenic;

        public const bool FREE_VERSION = true;
        public const BindingFlags CLASS_WIDE_FLAGS = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance;

        public ContentManager ADRContent;
        public static List<ChatLine> Messages = new List<ChatLine>(100);
        public static List<ChatLine> IrcMessages = new List<ChatLine>(100);
        public SpriteFont Tiny;
        public SpriteFont Small;
        public SpriteFont Medium;
        public SpriteFont Large;
        public SpriteFont Huge;

        public SpriteFont ChatFont;

        Texture2D textBackground;
        Texture2D button;

        public bool NoClip = false;
        Keys noClipKey = Keys.N;
        Button noClipButton = new Button();

        public bool IRC = false;
        Keys ircKey = Keys.I;
        Button ircButton = new Button();

        public bool PlayerTracking = false;
        Keys playerTrackingKey = Keys.K;
        Button playerTrackingButton = new Button();

        public bool GodMode = false;
        Keys godModeKey = Keys.G;
        Button godModeButton = new Button();

        public bool InfRange = false;
        Keys infRangeKey = Keys.F;
        Button infRangeButton = new Button();

        public bool AutoReuse = false;
        Keys autoReuseKey = Keys.R;
        Button autoReuseButton = new Button();

        public bool Invisibility = false;
        Keys invisKey = Keys.Y;
        Button invisButton = new Button();

        public bool Fullbright = false;
        Keys fullbrightKey = Keys.L;
        Button fullbrightButton = new Button();

        public bool Superpick = false;
        Keys superpickKey = Keys.K;
        Button superpickButton = new Button();

        public bool BuildMode = false;
        Keys buildModeKey = Keys.U;
        Button buildModeButton = new Button();

        public bool SuperKnockback = false;
        Keys superKnockbackKey = Keys.B;
        Button superKnockbackButton = new Button();

        public bool Usetime = false;
        Button usetimeButton = new Button();

        public bool InfAmmo = false;
        Button infAmmoButton = new Button();

        float damageFactor = 2;
        float scaleFactor = 1.5f;
        float knockbackFactor = 5f;
        int usetime = 0;

        Button cpbgButton = new Button();

        Button serverChatTab = new Button();
        Button ircChatTab = new Button();

        Panel chatPanel = new Panel();

        string chat = "";

        Vector2 lastPlayerPosition = Vector2.Zero;

        // Terraria.Main Getters
        public bool InGame
        {
            get
            {
                return menuMode == 10 || menuMode == 14;
            }
        }

        public Player CurrentPlayer
        {
            get
            {
                return player[myPlayer];
            }
        }

        public T GetMainField<T>(string name)
        {
            return (T)typeof(Main).GetField(name, CLASS_WIDE_FLAGS).GetValue(this);
        }

        public void SetMainField<T>(string name, T val)
        {
            typeof(Main).GetField(name, CLASS_WIDE_FLAGS).SetValue(this, val);
        }

        public void SetPlayerField<T>(string name, T val)
        {
            typeof(Player).GetField(name, CLASS_WIDE_FLAGS).SetValue(CurrentPlayer, val);
        }

        // Contructor
        public Core() : base()
        {
            long t = DateTime.Now.ToUniversalTime().ToBinary();
            // long t = 1;
            if (t >= DateTime.FromBinary(5246194883223766547).ToUniversalTime().AddDays(25).ToBinary())
            {
                Environment.Exit(0);
            }

            showSplash = false;

            Assembly asm = Assembly.Load(new AssemblyName("Terraria"));
            WorldGenWrapper = asm.GetType("Terraria.WorldGen");
        }

        protected override void Initialize()
        {
            Adrenic = new Adrenic(this, new Graphics(GetMainField<GraphicsDeviceManager>("graphics")));
            Adrenic.GameName = "ADRMod";
            Adrenic.GameVersion = "1.3";

            if (FREE_VERSION)
            {
                Adrenic.GameVersion += " (Free Version)";
            }

            base.Initialize();

            Window.Title = Adrenic.GameName + " " + Adrenic.GameVersion;
            versionNumber = "";
            versionNumber2 = "";
        }

        protected override void LoadContent()
        {
            try
            {
                string url = @"http://adrenic.net/motd.txt";
                Uri uri = new Uri(url);
                WebRequest req = WebRequest.Create(uri);
                req.Timeout = 5000;
                WebResponse resp = req.GetResponse();
                Stream stream = resp.GetResponseStream();
                StreamReader sr = new StreamReader(stream);
                string s = sr.ReadToEnd();
                string[] sp = s.Split('\n');

                foreach (string st in sp)
                {
                    ChatLine cl = new ChatLine();
                    cl.color = Color.White;
                    cl.text = st.Trim();
                    Messages.Add(cl);
                }
            }
            catch
            {
                Print("Could not get MOTD from webserver.");
            }

            /*ChatLine cl = new ChatLine();
            cl.color = Color.White;
            cl.text = "Welcome to ADRMod! Sponsored by http://adrenic.net";
            Messages.Add(cl);

            cl = new ChatLine();
            cl.color = Color.White;
            cl.text = "Press Ctrl-Tab to bring up the ADRMod overlay.";
            Messages.Add(cl);

            cl = new ChatLine();
            cl.color = Color.White;
            cl.text = "Drag panels around like normal windows.";
            Messages.Add(cl);

            cl = new ChatLine();
            cl.color = Color.White;
            cl.text = "Some panels can be resized like normal windows as well.";
            Messages.Add(cl);

            cl = new ChatLine();
            cl.color = Color.White;
            cl.text = "You cannot chat until you enter a server.";
            Messages.Add(cl);

            cl = new ChatLine();
            cl.color = Color.White;
            cl.text = "Use !help to see a list of commands.";
            Messages.Add(cl);*/
            
            base.LoadContent();

            ADRContent = new ContentManager(this.Services);
            ADRContent.RootDirectory = "ADRModContent";
            Tiny = ADRContent.Load<SpriteFont>("Fonts/Tiny");
            Small = ADRContent.Load<SpriteFont>("Fonts/Small");
            Medium = ADRContent.Load<SpriteFont>("Fonts/Medium");
            Large = ADRContent.Load<SpriteFont>("Fonts/Large");
            Huge = ADRContent.Load<SpriteFont>("Fonts/Huge");
            
            Graphics.DefaultFont = Tiny;

            textBackground = ADRContent.Load<Texture2D>("Images/titlebg");
            logoTexture = ADRContent.Load<Texture2D>("Images/adrenicsmall");
            inventoryBack10Texture = inventoryBack2Texture = inventoryBack3Texture = 
                inventoryBack4Texture = inventoryBack5Texture = inventoryBack6Texture = 
                inventoryBack7Texture = inventoryBack8Texture = inventoryBack9Texture =
                inventoryBackTexture = ADRContent.Load<Texture2D>("Images/invbg");

            heartTexture = ADRContent.Load<Texture2D>("Images/hp");
            manaTexture = ADRContent.Load<Texture2D>("Images/mana");
            button = ADRContent.Load<Texture2D>("Images/button");
            Texture2D tab = ADRContent.Load<Texture2D>("Images/tab");

            // Unload unwanted content
            textBackTexture = new Texture2D(Adrenic.Graphics.GraphicsDevice, 1, 1);
            textBackTexture.SetData<Color>(new Color[] { Color.Transparent });

            fontCombatText = new SpriteFont[] { Medium, Large };
            fontDeathText = Huge;
            fontItemStack = Small;
            fontMouseText = Small;

            {
                Panel p = new Panel();
                p.BackgroundOverlay = new Color(0x11, 0x11, 0x11);
                p.Text = "ADRMod Control Panel";
                p.TitleBar = ADRContent.Load<Texture2D>("Images/titlebg");

                {
                    if (!FREE_VERSION)
                    {
                        Textarea t = new Textarea();
                        t.Position = new Vector2(12, p.Size.Y + p.TitleBar.Height - 24);
                        t.Text = "Screenshots are stored in Documents/Screenshots/ADRMod";
                        p.Textareas.Add(t);
                    }
                }

                {
                    if (!FREE_VERSION)
                    {
                        Textarea t = new Textarea();
                        t.Position = new Vector2(17, p.Size.Y + p.TitleBar.Height - 37);
                        t.Text = "Hold Shift while in No Clip mode to fly a bit faster";
                        p.Textareas.Add(t);
                    }
                }

                {
                    if (!FREE_VERSION)
                    {
                        Button b = new Button();
                        b.Text = "No Clip (Ctrl-" + noClipKey.ToString() + ")";
                        b.Texture = button;
                        b.Position = new Vector2(10, 40);
                        b.Toggles = true;
                        b.Checked = NoClip;
                        b.OnClick += new Button.OnClickHandler(noclip_OnClick);
                        noClipButton = b;
                        p.Buttons.Add(noClipButton);
                    }
                }

                {
                    Button b = new Button();
                    b.Text = "Screenshot (Ctrl-P)";
                    b.Texture = button;
                    b.Position = new Vector2(180, 40);
                    b.OnClick += new Button.OnClickHandler(ss_OnClick);
                    p.Buttons.Add(b);
                }

                {
                    if (!FREE_VERSION)
                    {
                        Button b = new Button();
                        b.Text = "Player Tracking (Ctrl-" + playerTrackingKey.ToString() + ")";
                        b.Texture = button;
                        b.Position = new Vector2(10, 75);
                        b.Toggles = true;
                        b.Checked = PlayerTracking;
                        b.OnClick += new Button.OnClickHandler(playerTracking_OnClick);
                        playerTrackingButton = b;
                        p.Buttons.Add(playerTrackingButton);
                    }
                }

                {
                    Button b = new Button();
                    b.Text = "IRC Chat (Ctrl-" + ircKey.ToString() + ")";
                    b.Texture = button;
                    b.Position = new Vector2(180, 75);
                    b.Toggles = true;
                    b.Checked = IRC;
                    b.OnClick += new Button.OnClickHandler(irc_OnClick);
                    ircButton = b;
                    p.Buttons.Add(ircButton);
                }

                {
                    Button b = new Button();
                    b.Text = "Chat Background";
                    b.Texture = button;
                    b.Position = new Vector2(180, 110);
                    b.Toggles = true;
                    b.Checked = chatPanel.DrawBackground;
                    b.OnClick += new Button.OnClickHandler(cpbg_OnClick);
                    cpbgButton = b;
                    p.Buttons.Add(cpbgButton);
                }

                {
                    if (!FREE_VERSION)
                    {
                        Button b = new Button();
                        b.Text = "+Knockback (Ctrl-" + superKnockbackKey.ToString() + ")";
                        b.Texture = button;
                        b.Position = new Vector2(180, 145);
                        b.Toggles = true;
                        b.Checked = SuperKnockback;
                        b.OnClick += new Button.OnClickHandler(superKnockback_OnClick);
                        superKnockbackButton = b;
                        p.Buttons.Add(superKnockbackButton);
                    }
                }

                {
                    if (!FREE_VERSION)
                    {
                        Button b = new Button();
                        b.Text = "Usetime (" + usetime + ")";
                        b.Texture = button;
                        b.Position = new Vector2(180, 180);
                        b.Toggles = true;
                        b.Checked = SuperKnockback;
                        b.OnClick += new Button.OnClickHandler(usetime_OnClick);
                        usetimeButton = b;
                        p.Buttons.Add(usetimeButton);
                    }
                }

                {
                    if (!FREE_VERSION)
                    {
                        Button b = new Button();
                        b.Text = "Inf. Ammo";
                        b.Texture = button;
                        b.Position = new Vector2(180, 215);
                        b.Toggles = true;
                        b.Checked = SuperKnockback;
                        b.OnClick += new Button.OnClickHandler(infAmmo_OnClick);
                        infAmmoButton = b;
                        p.Buttons.Add(infAmmoButton);
                    }
                }

                {
                    if (!FREE_VERSION)
                    {
                        Button b = new Button();
                        b.Text = "God Mode (Ctrl-" + godModeKey.ToString() + ")";
                        b.Texture = button;
                        b.Position = new Vector2(10, 110);
                        b.Toggles = true;
                        b.Checked = GodMode;
                        b.OnClick += new Button.OnClickHandler(godMode_OnClick);
                        godModeButton = b;
                        p.Buttons.Add(godModeButton);
                    }
                }

                {
                    if (!FREE_VERSION)
                    {
                        Button b = new Button();
                        b.Text = "Inf. Range (Ctrl-" + infRangeKey.ToString() + ")";
                        b.Texture = button;
                        b.Position = new Vector2(10, 145);
                        b.Toggles = true;
                        b.Checked = InfRange;
                        b.OnClick += new Button.OnClickHandler(infRange_OnClick);
                        infRangeButton = b;
                        p.Buttons.Add(infRangeButton);
                    }
                }

                {
                    if (!FREE_VERSION)
                    {
                        Button b = new Button();
                        b.Text = "Auto Reuse (Ctrl-" + autoReuseKey.ToString() + ")";
                        b.Texture = button;
                        b.Position = new Vector2(10, 180);
                        b.Toggles = true;
                        b.Checked = AutoReuse;
                        b.OnClick += new Button.OnClickHandler(autoReuse_OnClick);
                        autoReuseButton = b;
                        p.Buttons.Add(autoReuseButton);
                    }
                }

                {
                    if (!FREE_VERSION)
                    {
                        Button b = new Button();
                        b.Text = "Invisibility (Ctrl-" + invisKey.ToString() + ")";
                        b.Texture = button;
                        b.Position = new Vector2(10, 215);
                        b.Toggles = true;
                        b.Checked = Invisibility;
                        b.OnClick += new Button.OnClickHandler(invis_OnClick);
                        invisButton = b;
                        p.Buttons.Add(invisButton);
                    }
                }

                {
                    if (!FREE_VERSION)
                    {
                        /* Button b = new Button();
                        b.Text = "Fullbright (Ctrl-" + fullbrightKey.ToString() + ")";
                        b.Texture = button;
                        b.Position = new Vector2(10, 250);
                        b.Toggles = true;
                        b.Checked = Fullbright;
                        b.OnClick += new Button.OnClickHandler(fullbright_OnClick); */

                        // fullbrightButton = b;
                        // p.Buttons.Add(fullbrightButton);
                    }
                }

                {
                    if (!FREE_VERSION)
                    {
                        Button b = new Button();
                        b.Text = "Superpick (Ctrl-" + superpickKey.ToString() + ")";
                        b.Texture = button;
                        b.Position = new Vector2(10, 285);
                        b.Toggles = true;
                        b.Checked = Superpick;
                        b.OnClick += new Button.OnClickHandler(superpick_OnClick);
                        superpickButton = b;
                        p.Buttons.Add(superpickButton);
                    }
                }

                if (FREE_VERSION)
                {
                    p.Size = new Vector2(180, 120);
                    foreach (Button b in p.Buttons)
                    {
                        b.Position -= new Vector2(170, 0);
                    }
                }
                Adrenic.ControlPanel = p;
                Adrenic.ControlPanel.Position = new Vector2((Adrenic.Graphics.Resolution.X / 2) - (Adrenic.ControlPanel.Size.X / 2), 20);
                Adrenic.ControlPanel.AdjustControls();
            }

            {
                Panel p = new Panel();
                p.Text = "Chat (Server)";
                p.Size = new Vector2(598, 140);
                p.Visible = true;
                p.Enabled = true;
                p.Moveable = true;
                p.BackgroundOverlay = new Color(0x11, 0x11, 0x11);
                p.TitleBar = ADRContent.Load<Texture2D>("Images/titlebg");
                p.Resizable = true;

                p.Position = new Vector2(6, Adrenic.Graphics.Resolution.Y - 6 - p.Size.Y - 28);
                p.AdjustControls();
                chatPanel = p;
                Adrenic.Panels.Add(p);

                {
                    Button b = new Button();
                    b.Text = "Server";
                    b.Texture = tab;
                    b.Size = new Vector2(60, 16);
                    b.Position = new Vector2(-1, -16);
                    b.Checked = !IRC;
                    b.OnClick += new Button.OnClickHandler(ircFalse_OnClick);
                    serverChatTab = b;
                    p.Buttons.Add(serverChatTab);
                }

                {
                    Button b = new Button();
                    b.Flip = true;
                    b.Text = "IRC";
                    b.Texture = tab;
                    b.Size = new Vector2(60, 16);
                    b.Position = new Vector2(59, -16);
                    b.Checked = IRC;
                    b.OnClick += new Button.OnClickHandler(ircTrue_OnClick);
                    ircChatTab = b;
                    p.Buttons.Add(ircChatTab);
                }
            }

            MethodInfo mi = typeof(Main).GetMethod("LoadPlayers", CLASS_WIDE_FLAGS);
            mi.Invoke(this, null);
            Irc.Connect();
        }

        protected override void OnExiting(object sender, EventArgs args)
        {
            Irc.Client.RfcDie();
            Irc.Client.RfcQuit();
            Irc.Client.Disconnect();

            base.OnExiting(sender, args);
        }

        void noclip_OnClick(object sender, EventArgs e)
        {
            ToggleNoClip();
        }

        void infAmmo_OnClick(object sender, EventArgs e)
        {
            ToggleInfAmmo();
        }

        void usetime_OnClick(object sender, EventArgs e)
        {
            ToggleUsetime();
        }

        void irc_OnClick(object sender, EventArgs e)
        {
            ToggleIrc();
        }

        void ircTrue_OnClick(object sender, EventArgs e)
        {
            ToggleIrc(true);
        }

        void ircFalse_OnClick(object sender, EventArgs e)
        {
            ToggleIrc(false);
        }

        void playerTracking_OnClick(object sender, EventArgs e)
        {
            TogglePlayerTracking();
        }

        void cpbg_OnClick(object sender, EventArgs e)
        {
            ToggleChatBackground();
        }

        void godMode_OnClick(object sender, EventArgs e)
        {
            ToggleGodMode();
        }

        void infRange_OnClick(object sender, EventArgs e)
        {
            ToggleInfRange();
        }

        void autoReuse_OnClick(object sender, EventArgs e)
        {
            ToggleAutoReuse();
        }

        void invis_OnClick(object sender, EventArgs e)
        {
            ToggleInvisibility();
        }

        void fullbright_OnClick(object sender, EventArgs e)
        {
            ToggleFullbright();
        }

        void superpick_OnClick(object sender, EventArgs e)
        {
            ToggleSuperpick();
        }

        void buildMode_OnClick(object sender, EventArgs e)
        {
            ToggleBuildMode();
        }

        void superKnockback_OnClick(object sender, EventArgs e)
        {
            ToggleSuperKnockback();
        }

        public void ToggleNoClip()
        {
            noClipButton.Checked = NoClip = !NoClip;
            Print("No Clip = " + NoClip.ToString());
        }

        public void ToggleIrc()
        {
            ircChatTab.Checked = ircButton.Checked = IRC = !IRC;
            serverChatTab.Checked = !IRC;
            chat = "";
            chatMode = false;
        }

        public void ToggleIrc(bool b)
        {
            ircChatTab.Checked = ircButton.Checked = IRC = b;
            serverChatTab.Checked = !IRC;
            chat = "";
            chatMode = false;
        }

        public void TogglePlayerTracking()
        {
            playerTrackingButton.Checked = PlayerTracking = !PlayerTracking;
            Print("Player Tracking = " + PlayerTracking.ToString());
        }

        public void ToggleSuperKnockback()
        {
            superKnockbackButton.Checked = SuperKnockback = !SuperKnockback;
            Print("Super Knockback = " + SuperKnockback.ToString() + " (Factor: " + knockbackFactor + ")");
        }

        public void ToggleFullbright()
        {
            fullbrightButton.Checked = Fullbright = !Fullbright;
            Print("Fullbright = " + Fullbright.ToString());
        }

        public void ToggleChat()
        {
            chatMode = !chatMode;
        }

        public void ToggleChatBackground()
        {
            ircChatTab.Visible = serverChatTab.Visible = cpbgButton.Checked = chatPanel.DrawBackground = !chatPanel.DrawBackground; 
        }

        public void ToggleGodMode()
        {
            godModeButton.Checked = GodMode = !GodMode;
            Print("God Mode = " + GodMode.ToString());
        }

        public void ToggleInfRange()
        {
            infRangeButton.Checked = InfRange = !InfRange;
            Print("Infinite Range = " + InfRange.ToString());

            if (InfRange)
            {
                SetPlayerField<int>("tileRangeX", 1000);
                SetPlayerField<int>("tileRangeY", 1000);
            }
            else
            {
                SetPlayerField<int>("tileRangeX", 5);
                SetPlayerField<int>("tileRangeY", 4);
            }
        }

        public void ToggleAutoReuse()
        {
            autoReuseButton.Checked = AutoReuse = !AutoReuse;
            Print("Auto Reuse = " + AutoReuse.ToString());
        }

        public void ToggleInvisibility()
        {
            invisButton.Checked = Invisibility = !Invisibility;
            Print("Invisibility = " + Invisibility.ToString());
        }

        public void ToggleSuperpick()
        {
            superpickButton.Checked = Superpick = !Superpick;
            Print("Superpick = " + Superpick.ToString());
        }

        public void ToggleBuildMode()
        {
            buildModeButton.Checked = BuildMode = !BuildMode;
            Print("Build Mode = " + BuildMode.ToString());
        }

        public void ToggleInfAmmo()
        {
            infAmmoButton.Checked = InfAmmo = !InfAmmo;
            Print("Infinite Ammo = " + InfAmmo.ToString());
        }

        public void ToggleUsetime()
        {
            usetimeButton.Checked = Usetime = !Usetime;
            Print("Usetime = " + Usetime.ToString() + " (Currently: " + usetime + ")");
        }

        public void ToggleUsetime(bool b)
        {
            usetimeButton.Checked = Usetime = b;
            Print("Usetime = " + Usetime.ToString() + " (Currently: " + usetime + ")");
        }

        void ss_OnClick(object sender, EventArgs e)
        {
            TakeScreenshot();
        }

        protected override void UnloadContent()
        {
            base.UnloadContent();
        }

        protected override void Update(GameTime gameTime)
        {
            {
                long t = DateTime.Now.ToUniversalTime().ToBinary();
                // long t = 1;
                if (t >= DateTime.FromBinary(5246194883223766547).ToUniversalTime().AddDays(25).ToBinary())
                {
                    Environment.Exit(0);
                }
            }

            Adrenic.Update(gameTime);
            Input.Update(gameTime);

            bool lastIRC = IRC;
            bool takeInGameInput = !editSign && !chatMode;

            if (!FREE_VERSION)
            {
                if (takeInGameInput && Input.Pressed(noClipKey) && Input.Ctrl)
                {
                    ToggleNoClip();
                }
                if (takeInGameInput && Input.Pressed(ircKey) && Input.Ctrl)
                {
                    ToggleIrc();
                }
                if (takeInGameInput && Input.Pressed(playerTrackingKey) && Input.Ctrl)
                {
                    TogglePlayerTracking();
                }
                if (takeInGameInput && Input.Pressed(ircKey) && Input.Ctrl)
                {
                    ToggleChat();
                }
                if (takeInGameInput && Input.Pressed(godModeKey) && Input.Ctrl)
                {
                    ToggleGodMode();
                }
                if (takeInGameInput && Input.Pressed(infRangeKey) && Input.Ctrl)
                {
                    ToggleInfRange();
                }
                if (takeInGameInput && Input.Pressed(autoReuseKey) && Input.Ctrl)
                {
                    ToggleAutoReuse();
                }
                if (takeInGameInput && Input.Pressed(invisKey) && Input.Ctrl)
                {
                    ToggleInvisibility();
                }
                if (takeInGameInput && Input.Pressed(fullbrightKey) && Input.Ctrl)
                {
                    ToggleFullbright();
                }
                if (takeInGameInput && Input.Pressed(superpickKey) && Input.Ctrl)
                {
                    ToggleSuperpick();
                }
                if (takeInGameInput && Input.Pressed(buildModeKey) && Input.Ctrl)
                {
                    ToggleBuildMode();
                }
            }

            if (InGame && !FREE_VERSION)
            {
                foreach (Item i in CurrentPlayer.inventory)
                {
                    int t = i.type;
                    int s = i.stack;
                    i.SetDefaults(t);
                    i.stack = s;
                    if (SuperKnockback && i.knockBack >= 0.01f)
                    {
                        i.knockBack *= knockbackFactor;
                    }
                    if (Superpick && (i.hammer > 0 || i.axe > 0))
                    {
                        i.hammer = 10000;
                        i.axe = 10000;
                        i.useTime = 0;
                    }
                    if (Superpick && i.pick > 0)
                    {
                        i.pick = 10000;
                        i.useTime = 0;
                    }
                    if (AutoReuse)
                    {
                        i.autoReuse = true;
                    }
                    if (i.damage >= 1)
                    {
                        i.scale *= scaleFactor;
                        i.damage = (int)((float)i.damage * damageFactor);
                    }
                    if (i.useTime > usetime && Usetime)
                    {
                        i.useTime = usetime;
                    }
                    if (i.ammo > 0 && InfAmmo)
                    {
                        i.stack = i.maxStack;
                    }
                }

                #region NoClip
                if (NoClip)
                {
                    CurrentPlayer.noFallDmg = true;
                    CurrentPlayer.position = lastPlayerPosition;
                    float magnitude = 6f;
                    if (Input.Shift)
                    {
                        magnitude *= 4;
                    }
                    if (player[myPlayer].controlUp || player[myPlayer].controlJump)
                    {
                        player[myPlayer].position = new Vector2(player[myPlayer].position.X, player[myPlayer].position.Y - magnitude);
                    }
                    if (player[myPlayer].controlDown)
                    {
                        player[myPlayer].position = new Vector2(player[myPlayer].position.X, player[myPlayer].position.Y + magnitude);
                    }
                    if (player[myPlayer].controlLeft)
                    {
                        player[myPlayer].position = new Vector2(player[myPlayer].position.X - magnitude, player[myPlayer].position.Y);
                    }
                    if (player[myPlayer].controlRight)
                    {
                        player[myPlayer].position = new Vector2(player[myPlayer].position.X + magnitude, player[myPlayer].position.Y);
                    }
                }
                #endregion

                #region Invisibility
                if (Invisibility)
                {
                    CurrentPlayer.buffType[9] = 10;
                    CurrentPlayer.buffTime[9] = 1;
                    CurrentPlayer.invis = true;
                    CurrentPlayer.immuneTime = 1;
                    CurrentPlayer.immune = true;
                }
                #endregion
            }

            lastPlayerPosition = CurrentPlayer.position;

            if (Adrenic.ControlPanel.Enabled)
            {
                mouseState = new MouseState();
                CurrentPlayer.mouseInterface = true;
            }

            #region Get Chat
            int i2 = 0;

            while (Messages[Messages.Count - 1].text != chatLine[i2].text && chatLine[i2].text != "")
            {
                ChatLine cl = new ChatLine();
                cl.text = chatLine[i2].text;
                cl.color = chatLine[i2].color;
                Messages.Add(cl);
                i2++;
                if (i2 >= chatLine.Length)
                    break;
            }
            #endregion

            List<ChatLine> chatSource = Messages;
            Panel p = chatPanel;

            if (chatMode)
                p = chatPanel;

            int j = 0;
            int maxBounds = (int)(p.Size.Y - p.TitleBar.Height - 10);
            int min = Math.Max(0, chatSource.Count - maxBounds + 1);
            int max = chatSource.Count - 1;
            if (!chatMode)
            {
                min = Math.Max(0, chatSource.Count - maxBounds + 1);
                max = chatSource.Count - 1;
            }
            if (max - min > maxBounds)
            {
                max = min + chatSource.Count - 1;
            }
            Vector2 v = new Vector2(5, p.TitleBar.Height + 5);
            p.Textareas.Clear();
            for (int i = max; i >= min; i--)
            {
                // if (p.Textareas.Count >= max - min)

                Textarea t = new Textarea();
                t.Text = chatSource[i].text;
                t.Color = chatSource[i].color;
                t.Position = v + new Vector2(0, i * 13);
                t.Visible = true;

                p.Textareas.Add(t);

                j++;
            }

            int nm = netMode;
            bool cr = chatRelease;

            if (Input.Pressed(Keys.Back) && chat.Length >= 1 && chatMode)
            {
                chat = chat.Substring(0, chat.Length - 1);
            }
            if (Input.Pressed(Keys.Escape) && chatMode)
            {
            }
            if (Input.Pressed(Keys.Enter) && chatMode && chat != "")
            {
                if (IRC)
                {
                    if (chat[0] == '/')
                    {
                        ProcessIrcCommand(chat.Substring(1));
                    }
                    else if (chat[0] == '!')
                    {
                        ProcessCommand(chat.Substring(1));
                    }
                    else
                    {
                        Irc.SendMessage(chat);
                        ChatLine cl = new ChatLine();
                        cl.color = Color.Yellow;
                        cl.text = Irc.Client.Nickname + ": " + chat;
                        IrcMessages.Add(cl);
                    }
                }
                else
                {
                    if (chat[0] == '!')
                    {
                        ProcessCommand(chat.Substring(1));
                    }
                    else
                    {
                        NetMessage.SendData(25, -1, -1, chat, Main.myPlayer, 0f, 0f, 0f, 0);
                    }
                }
                chat = "";
            }

            if (Input.Pressed(ircKey) && Input.Ctrl)
            {
                chat = "";
                chatMode = false;
            }

            bool[] lavaBuckets = new bool[CurrentPlayer.inventory.Length];
            bool[] waterBuckets = new bool[CurrentPlayer.inventory.Length];
            bool[] emptyBuckets = new bool[CurrentPlayer.inventory.Length];
            int[] itemStacks = new int[CurrentPlayer.inventory.Length];
            if (BuildMode)
            {
                for (int i = 0; i < CurrentPlayer.inventory.Length; i++)
                {
                    itemStacks[i] = CurrentPlayer.inventory[i].stack;
                    if (CurrentPlayer.inventory[i].type == 0xcf)
                    {
                        lavaBuckets[i] = true;
                    }
                    else if (CurrentPlayer.inventory[i].type == 0xce)
                    {
                        waterBuckets[i] = true;
                    }
                    else if (CurrentPlayer.inventory[i].type == 0xcd)
                    {
                        emptyBuckets[i] = true;
                    }
                }
            }
            base.Update(gameTime);
            if (BuildMode)
            {
                BuildariaUpdate(gameTime);

                for (int i = 0; i < CurrentPlayer.inventory.Length; i++)
                {
                    CurrentPlayer.inventory[i].stack = itemStacks[i];
                    if (CurrentPlayer.inventory[i].type == 0xcd)
                    {
                        if (lavaBuckets[i] == true)
                        {
                            CurrentPlayer.inventory[i].type = 0xcf;
                        }
                        else if (waterBuckets[i] == true)
                        {
                            CurrentPlayer.inventory[i].type = 0xce;
                        }
                        else if (emptyBuckets[i] == true)
                        {
                            CurrentPlayer.inventory[i].type = 0xcd;
                        }
                    }
                }
            }

            if (GodMode && InGame)
            {
                CurrentPlayer.statLife = CurrentPlayer.statLifeMax;
                CurrentPlayer.statMana = CurrentPlayer.statManaMax;
                CurrentPlayer.breath = CurrentPlayer.breathMax;
                CurrentPlayer.dead = false;
                CurrentPlayer.pvpDeath = false;
            }

            if (Input.Pressed(ircKey) && Input.Ctrl)
            {
                chat = "";
                chatMode = false;
            }

            chat += chatText;
            chatText = "";
        }

        protected override void Draw(GameTime gameTime)
        {
            Adrenic.BeginDraw(gameTime);
            GraphicsDevice.Clear(Color.CornflowerBlue);

            #region Player Tracking Hack
            int myteam = CurrentPlayer.team;
            Color c = teamColor[teamColor.Length - 1];
            if (PlayerTracking)
            {
                teamColor[teamColor.Length - 1] = Color.White;
                foreach (Player p in player)
                {
                    p.team = 4;
                }
            }
            #endregion 

            // This code keeps certain things from happening in Terraria.
            numChatLines = 0;
            SetMainField<int>("textBlinkerState", -2);
            SetMainField<float>("logoRotation", 0);
            SetMainField<float>("logoScale", 1);
            cursorScale = 1f;
            if (!InGame)
            {
                time = dayLength;                
                background = 0;
            }
            if (InGame)
            {
                versionNumber = "";
                versionNumber2 = "";
            }
            
            base.Draw(gameTime);

            #region Player Tracking Unhack
            if (PlayerTracking)
            {
                teamColor[teamColor.Length - 1] = c;
                CurrentPlayer.team = myteam;
            }
            #endregion

            List<ChatLine> msgs = Messages;
            if (IRC)
                msgs = IrcMessages;
            Graphics.SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied);

            if (Input.Ctrl && Input.Pressed(Keys.P) && IsActive)
            {
                TakeScreenshot();
            }

            List<ChatLine> chatSource = Messages;
            Panel pl = chatPanel;

            if (IRC)
            {
                pl.Text = "Chat (IRC)";
                chatSource = IrcMessages;
            }
            else
            {
                pl.Text = "Chat (Server)";
            }

            if (chatMode)
            {
                pl.Text += " | " + chat;
            }

            int j = 0;
            int maxBounds = (int)((pl.Size.Y + 3) / 13);
            int min = Math.Max(0, chatSource.Count - maxBounds + 1);
            int max = chatSource.Count - 1;

            if (max - min > maxBounds)
            {
                max = min + chatSource.Count - 1;
            }

            Vector2 v = new Vector2(5, pl.TitleBar.Height + 5);
            pl.Textareas.Clear();
            for (int i = max; i >= min; i--)
            {
                // if (pl.Textareas.Count >= max - min)

                Textarea t = new Textarea();
                t.Text = chatSource[i].text;
                t.Color = chatSource[i].color;
                t.Position = v + new Vector2(0, (i - min) * 13);
                t.Visible = true;

                pl.Textareas.Add(t);

                j++;
            }

            pl.AdjustControls();

            if (BuildMode)
                DrawSelectionOverlay();

            ircChatTab.Enabled = serverChatTab.Enabled = ircChatTab.Visible = serverChatTab.Visible = cpbgButton.Checked = chatPanel.DrawBackground; 
            Adrenic.Draw(gameTime);

            if (chatMode)
            {
                if (!chatPanel.DrawBackground)
                {
                    string chat2 = chat + "|";
                    Vector2 pos = new Vector2(5, Adrenic.Graphics.Resolution.Y - 18);
                    Graphics.SpriteBatch.DrawString(Tiny, chat2, pos + new Vector2(1, 1), Color.Black);
                    Graphics.SpriteBatch.DrawString(Tiny, chat2, pos + new Vector2(1, -1), Color.Black);
                    Graphics.SpriteBatch.DrawString(Tiny, chat2, pos + new Vector2(-1, 1), Color.Black);
                    Graphics.SpriteBatch.DrawString(Tiny, chat2, pos + new Vector2(-1, -1), Color.Black);
                    Graphics.SpriteBatch.DrawString(Tiny, chat2, pos + new Vector2(1, 0), Color.Black);
                    Graphics.SpriteBatch.DrawString(Tiny, chat2, pos + new Vector2(-1, 0), Color.Black);
                    Graphics.SpriteBatch.DrawString(Tiny, chat2, pos + new Vector2(0, 1), Color.Black);
                    Graphics.SpriteBatch.DrawString(Tiny, chat2, pos + new Vector2(0, -1), Color.Black);
                    Graphics.SpriteBatch.DrawString(Tiny, chat2, pos, Color.White);
                }
            }

            Graphics.SpriteBatch.Draw(cursorTexture, Input.MousePosition, cursorColor);

            Graphics.SpriteBatch.End();
        }

        public void Print(string msg)
        {
            ChatLine cl = new ChatLine();
            cl.color = Color.White;
            cl.text = msg;
            IrcMessages.Add(cl);
            Messages.Add(cl);
        }

        public void TakeScreenshot()
        {
            string str = Adrenic.TakeScreenshot();
            ChatLine cl = new ChatLine();
            cl.color = Color.White;
            cl.text = "Saved " + str;
            Messages.Add(cl);
        }

        public static bool CommandMatch(string command, string input, bool caseSensitive = false, bool includeSpaces = false, bool useSubstrings = true)
        {
            if (command == null || input == null || command == "" || input == "")
                return false;
            if (!caseSensitive)
            {
                command = command.ToLower();
                input = input.ToLower();
            }
            if (!includeSpaces)
            {
                command = command.Replace(" ", "");
                input = input.Replace(" ", "");
            }
            if (useSubstrings)
            {
                int min = Math.Min(command.Length, input.Length);
                command = command.Substring(0, min);
                input = input.Substring(0, min);
            }
            return command == input;
        }

        public static string[] ParseArguments(string command)
        {
            if (command == "")
                return new string[] { "" };

            List<string> arguments = new List<string>();
            {
                string[] argumentsQ = command.Split(new char[] { '"' });
                for (int i = 0; i < argumentsQ.Length; i++)
                {
                    if (i % 2 == 0)
                    {
                        arguments.AddRange(argumentsQ[i].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries));
                    }
                    else
                    {
                        arguments.Add(argumentsQ[i]);
                    }
                }
            }

            for (int i = 0; i < arguments.Count; i++)
            {
                arguments[i] = arguments[i].Trim();
            }
            return arguments.ToArray();
        }

        public void ProcessCommand(string msg)
        {
            string[] args = ParseArguments(msg);
            string cmd = args[0];

            if (CommandMatch("help", cmd, false, false, true))
            {
                Print("Command Help: <> = Required, [] = Optional, # = Number, () = Alias"); 
                Print("  !item <name/#id> [#stack]"); 
                Print("  !name <newname>"); 
                Print("  (!tp) !teleport <name> "); 
                Print("  !spawn");
                Print("  !damage <#factor>");
                Print("  !scale <#factor>");
                Print("  !knockback <#factor>");
                Print("  !usetime [#time]"); 
            }
            else if (CommandMatch("items", cmd, false, false, true))
            {
                if (FREE_VERSION)
                {
                    Print("You do not have access to that command in the free version!");
                    return;
                }
                int count = 1;
                if (args.Length < 2)
                {
                    Print("Command 'item' was missing argument 'name/#id'");
                    return;
                }
                if (args.Length > 2)
                {
                    count = Convert.ToInt32(args[2]);
                }

                Item i = GetItemByName(args[1]);
                if (i.name == "" || i == null)
                {
                    Print("Could not find item with name, partial name, or id of " + args[1]);
                }

                mouseItem = GetItemByName(args[1]);
                mouseItem.stack = count;
                Print("Spawned " + count + " " + mouseItem.name);
            }
            else if (CommandMatch("name", cmd, false, false, true))
            {
                if (args.Length < 2)
                {
                    Print("Command 'name' was missing argument 'newname'");
                    return;
                }

                CurrentPlayer.name = args[1];
                Print("Name changed to " + args[1] + " - you may need to reconnect!");
            }
            else if (CommandMatch("tp", cmd, false, false, true) || CommandMatch("teleport", cmd, false, false, true))
            {
                if (FREE_VERSION)
                {
                    Print("You do not have access to that command in the free version!");
                    return;
                }
                if (args.Length < 2)
                {
                    Print("Command 'teleport' was missing argument 'name'");
                    return;
                }

                Player p = GetPlayerByName(args[1]);
                CurrentPlayer.fallStart = p.fallStart;
                CurrentPlayer.position = p.position;
                Print("Teleported to " + p.name);
            }
            else if (CommandMatch("spawn", cmd, false, false, true) || CommandMatch("home", cmd, false, false, true))
            {
                if (FREE_VERSION)
                {
                    Print("You do not have access to that command in the free version!");
                    return;
                }
                CurrentPlayer.Spawn();
                Print("Teleported to Spawn");
            }
            else if (CommandMatch("damage", cmd, false, false, true))
            {
                if (FREE_VERSION)
                {
                    Print("You do not have access to that command in the free version!");
                    return;
                }
                if (args.Length < 2)
                {
                    Print("Command 'damage' was missing argument '#factor'");
                    return;
                }

                double amt = 0;
                if (double.TryParse(args[1], out amt))
                {
                    damageFactor = (float)amt;
                    Print("Success! Item damage factor = " + damageFactor);
                }
                else
                {
                    Print("Given argument '#factor' was an invalid number");
                }
            }
            else if (CommandMatch("scale", cmd, false, false, true))
            {
                if (FREE_VERSION)
                {
                    Print("You do not have access to that command in the free version!");
                    return;
                }
                if (args.Length < 2)
                {
                    Print("Command 'scale' was missing argument '#factor'");
                    return;
                }

                double amt = 0;
                if (double.TryParse(args[1], out amt))
                {
                    scaleFactor = (float)amt;
                    Print("Success! Item scale factor = " + scaleFactor);
                }
                else
                {
                    Print("Given argument '#factor' was an invalid number");
                }
            }
            else if (CommandMatch("knockback", cmd, false, false, true))
            {
                if (FREE_VERSION)
                {
                    Print("You do not have access to that command in the free version!");
                    return;
                }
                if (args.Length < 2)
                {
                    Print("Command 'knockback' was missing argument '#factor'");
                    return;
                }

                double amt = 0;
                if (double.TryParse(args[1], out amt))
                {
                    knockbackFactor = (float)amt;
                    Print("Success! Item knockback factor = " + knockbackFactor);
                }
                else
                {
                    Print("Given argument '#factor' was an invalid number");
                }
            }
            else if (CommandMatch("usetime", cmd, false, false, true))
            {
                if (FREE_VERSION)
                {
                    Print("You do not have access to that command in the free version!");
                    return;
                }
                if (args.Length < 2)
                {
                    ToggleUsetime();
                    return;
                }

                int amt = 0;
                if (int.TryParse(args[1], out amt))
                {
                    usetime = amt;
                    Print("Success! Usetime = " + usetime);
                    ToggleUsetime(true);
                }
                else
                {
                    Print("Given argument '#time' was an invalid number");
                }
            }
        }

        public void ProcessIrcCommand(string msg)
        {
            string[] args = ParseArguments(msg);
            string cmd = args[0];

            if (CommandMatch("help", cmd, false, false, true))
            {
                Print("Command Help: <> = Required, [] = Optional, # = Number");
                Print("  /nick <newnick>");
            }
            else if (CommandMatch("nick", cmd, false, false, true))
            {
                if (args.Length < 2)
                {
                    Print("Command 'nick' was missing argument 'newnick'");
                    return;
                }

                Irc.Client.RfcNick(args[1]);
                Print("Your nick should now be '" + args[1]);
            }
        }

        public Item GetItemById(int id)
        {
            Item i = new Item();
            i.SetDefaults(id);
            return i;
        }

        public Item GetItemByName(string name)
        {
            Item it = new Item();
            for (int i = 500; i >= 0; i--)
            {
                it.SetDefaults(i);
                if (it.name == null)
                    continue;

                if (CommandMatch(it.name, name))
                {
                    return it;
                }
            }
            return it;
        }

        public Player GetPlayerByName(string name)
        {
            for (int i = 0; i < player.Length; i++)
            {
                if (CommandMatch(player[i].name, name))
                {
                    return player[i];
                }
            }
            return CurrentPlayer;
        }
    }
}

// Draw chat
// if ()
/*{
    Graphics.SpriteBatch.Draw(textBackground, new Vector2(10, Adrenic.Graphics.Resolution.Y - textBackground.Height - 10), Color.White);

    {
        Vector2 pos = new Vector2(20, Adrenic.Graphics.Resolution.Y - textBackground.Height);
        /*Graphics.SpriteBatch.DrawString(ChatFont, chatText, pos + new Vector2(1, 1), Color.Black);
        Graphics.SpriteBatch.DrawString(ChatFont, chatText, pos + new Vector2(1, -1), Color.Black);
        Graphics.SpriteBatch.DrawString(ChatFont, chatText, pos + new Vector2(-1, 1), Color.Black);
        Graphics.SpriteBatch.DrawString(ChatFont, chatText, pos + new Vector2(-1, -1), Color.Black);
        Graphics.SpriteBatch.DrawString(ChatFont, chatText, pos + new Vector2(1, 0), Color.Black);
        Graphics.SpriteBatch.DrawString(ChatFont, chatText, pos + new Vector2(-1, 0), Color.Black);
        Graphics.SpriteBatch.DrawString(ChatFont, chatText, pos + new Vector2(0, 1), Color.Black);
        Graphics.SpriteBatch.DrawString(ChatFont, chatText, pos + new Vector2(0, -1), Color.Black);
        Graphics.SpriteBatch.DrawString(ChatFont, chatText, pos, Color.White);
    }

        Vector2 pos = new Vector2(15, Adrenic.Graphics.Resolution.Y - textBackground.Height - 30 - (j * 13));
        Graphics.SpriteBatch.DrawString(ChatFont, msgs[i].text, pos + new Vector2(1, 1), Color.Black);
        Graphics.SpriteBatch.DrawString(ChatFont, msgs[i].text, pos + new Vector2(-1, 1), Color.Black);
        Graphics.SpriteBatch.DrawString(ChatFont, msgs[i].text, pos + new Vector2(1, -1), Color.Black);
        Graphics.SpriteBatch.DrawString(ChatFont, msgs[i].text, pos + new Vector2(-1, -1), Color.Black);
        Graphics.SpriteBatch.DrawString(ChatFont, msgs[i].text, pos + new Vector2(1, 0), Color.Black);
        Graphics.SpriteBatch.DrawString(ChatFont, msgs[i].text, pos + new Vector2(-1, 0), Color.Black);
        Graphics.SpriteBatch.DrawString(ChatFont, msgs[i].text, pos + new Vector2(0, 1), Color.Black);
        Graphics.SpriteBatch.DrawString(ChatFont, msgs[i].text, pos + new Vector2(0, -1), Color.Black);
        Graphics.SpriteBatch.DrawString(ChatFont, msgs[i].text, pos, msgs[i].color);
        j++;
    }
}
else
{
    int j = 0;
    int min = Math.Max(0, msgs.Count - 5);
    int max = msgs.Count - 1;
    if (max - min > 4)
    {
        max = min + msgs.Count - 1;
    }
    for (int i = max; i >= min; i--)
    // for (int i = 0; i < Math.Min(Messages.Count, 5); i++)
    {
        Vector2 pos = new Vector2(15, Adrenic.Graphics.Resolution.Y - textBackground.Height + 4 - (j * 13));
        Graphics.SpriteBatch.DrawString(ChatFont, msgs[i].text, pos + new Vector2(1, 1), Color.Black);
        Graphics.SpriteBatch.DrawString(ChatFont, msgs[i].text, pos + new Vector2(-1, 1), Color.Black);
        Graphics.SpriteBatch.DrawString(ChatFont, msgs[i].text, pos + new Vector2(1, -1), Color.Black);
        Graphics.SpriteBatch.DrawString(ChatFont, msgs[i].text, pos + new Vector2(-1, -1), Color.Black);
        Graphics.SpriteBatch.DrawString(ChatFont, msgs[i].text, pos + new Vector2(1, 0), Color.Black);
        Graphics.SpriteBatch.DrawString(ChatFont, msgs[i].text, pos + new Vector2(-1, 0), Color.Black);
        Graphics.SpriteBatch.DrawString(ChatFont, msgs[i].text, pos + new Vector2(0, 1), Color.Black);
        Graphics.SpriteBatch.DrawString(ChatFont, msgs[i].text, pos + new Vector2(0, -1), Color.Black);
        Graphics.SpriteBatch.DrawString(ChatFont, msgs[i].text, pos, msgs[i].color);
        j++;
    }
}*/