﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using AdrenicLib;

using Terraria;

namespace ADRMod
{
    public partial class Core
    {
        Vector2 sel1 = Vector2.Zero;
        Vector2 sel2 = Vector2.Zero;

        Point SelectionSize = new Point(0, 0);
        Point SelectionPosition = new Point(0, 0);
        bool[,] SelectedTiles = new bool[1, 1];

        Point CopiedSize = new Point(0, 0);
        Tile[,] Copied = new Tile[1, 1];

        Point UndoSize = new Point(0, 0);
        Point UndoPosition = new Point(0, 0);
        Tile[,] Undo = new Tile[1, 1];

        Color SelectionOverlay = new Color(255, 100, 0, 50);
        Type WorldGenWrapper;

        public void TileFrame(int x, int y, bool reset = false, bool breaks = true)
        {
            // Accesses the WorldGen's TileFrame() method for keeping tiles looking presentable when placed with hax
            WorldGenWrapper.GetMethod("TileFrame").Invoke(null, new object[] { x, y, reset, !breaks });
        }

        public void SquareWallFrame(int x, int y, bool reset = false)
        {
            // It's the above, but for walls
            WorldGenWrapper.GetMethod("SquareWallFrame").Invoke(null, new object[] { x, y, reset });
        }

        public void BuildariaUpdate(GameTime gameTime)
        {
            if (mouseState.LeftButton == ButtonState.Pressed && player[myPlayer].inventory[player[myPlayer].selectedItem].createTile >= 0)
            {
                int x = (int)((Main.mouseState.X + Main.screenPosition.X) / 16f);
                int y = (int)((Main.mouseState.Y + Main.screenPosition.Y) / 16f);

                if (Main.tile[x, y].active == false)
                {
                    byte wall = Main.tile[x, y].wall;
                    Main.tile[x, y] = new Tile();
                    Main.tile[x, y].type = (byte)player[myPlayer].inventory[player[myPlayer].selectedItem].createTile;
                    Main.tile[x, y].wall = wall;
                    Main.tile[x, y].active = true;
                    TileFrame(x, y);
                    SquareWallFrame(x, y, true);
                }
            }
            else if (mouseState.LeftButton == ButtonState.Pressed && player[myPlayer].inventory[player[myPlayer].selectedItem].createWall >= 0)
            {
                int x = (int)((Main.mouseState.X + Main.screenPosition.X) / 16f);
                int y = (int)((Main.mouseState.Y + Main.screenPosition.Y) / 16f);

                if (Main.tile[x, y].wall == 0)
                {
                    if (Main.tile[x, y] == null)
                    {
                        Main.tile[x, y] = new Tile();
                        Main.tile[x, y].type = 0;
                    }

                    Main.tile[x, y].wall = (byte)player[myPlayer].inventory[player[myPlayer].selectedItem].createWall;
                    TileFrame(x, y);
                    SquareWallFrame(x, y, true);
                }
            }

            UpdateSelection();

            #region Alt For Circles

            if (Input.Alt && mouseState.LeftButton == ButtonState.Released)
            {
                for (int x = 0; x < SelectionSize.X; x++)
                {
                    for (int y = 0; y < SelectionSize.Y; y++)
                    {
                        SelectedTiles[x, y] = false;
                    }
                }
                Vector2 center = new Vector2(SelectionSize.X / 2f, SelectionSize.Y / 2f);
                for (int x = 0; x < SelectionSize.X; x++)
                {
                    for (int y = 0; y < SelectionSize.Y; y++)
                    {
                        double dx = (x - center.X + 1) / center.X;
                        double dy = (y - center.Y + 1) / center.Y;
                        if (dx * dx + dy * dy < 1)
                        {
                            SelectedTiles[x, y] = true;
                        }
                    }
                }
            }

            #endregion

            #region Shift For Outline

            if (Input.Shift && mouseState.LeftButton == ButtonState.Released)
            {
                bool[,] tempTiles = new bool[SelectionSize.X, SelectionSize.Y];
                for (int x = 0; x < SelectionSize.X; x++)
                {
                    for (int y = 0; y < SelectionSize.Y; y++)
                    {
                        tempTiles[x, y] = SelectedTiles[x, y];
                    }
                }
                for (int x = 0; x < SelectionSize.X; x++)
                {
                    bool found1 = false;
                    bool found2 = false;
                    for (int y = 0; y < SelectionSize.Y; y++)
                    {
                        if (!found1)
                        {
                            found1 = SelectedTiles[x, y];
                            continue;
                        }
                        else if (!found2)
                        {
                            if (y + 1 > SelectionSize.Y - 1)
                            {
                                found2 = SelectedTiles[x, y];
                                break;
                            }
                            else if (!found2 && !SelectedTiles[x, y + 1])
                            {
                                found2 = SelectedTiles[x, y];
                                break;
                            }
                            else
                            {
                                SelectedTiles[x, y] = false;
                            }
                        }
                        else if (found1 && found2)
                            break;
                    }
                }
                for (int y = 0; y < SelectionSize.Y; y++)
                {
                    bool found1 = false;
                    bool found2 = false;
                    for (int x = 0; x < SelectionSize.X; x++)
                    {
                        if (!found1)
                        {
                            found1 = tempTiles[x, y];
                            continue;
                        }
                        else if (!found2)
                        {
                            if (x + 1 > SelectionSize.X - 1)
                            {
                                found2 = tempTiles[x, y];
                                break;
                            }
                            else if (!found2 && !tempTiles[x + 1, y])
                            {
                                found2 = tempTiles[x, y];
                                break;
                            }
                            else
                            {
                                tempTiles[x, y] = false;
                            }
                        }
                        else if (found1 && found2)
                            break;
                    }
                }
                for (int x = 0; x < SelectionSize.X; x++)
                {
                    for (int y = 0; y < SelectionSize.Y; y++)
                    {
                        SelectedTiles[x, y] = SelectedTiles[x, y] || tempTiles[x, y];
                    }
                }
            }

            #endregion

            #region Selection Modifications

            #region Copy/Paste

            if (Input.Ctrl && Input.OldKeyboard.IsKeyUp(Keys.C) && Input.Down(Keys.C) && !editSign)
            {
                Copied = new Tile[SelectionSize.X, SelectionSize.Y];
                CopiedSize = new Point(SelectionSize.X, SelectionSize.Y);
                for (int x = 0; x < SelectionSize.X; x++)
                {
                    for (int y = 0; y < SelectionSize.Y; y++)
                    {
                        int copyX = x;
                        int copyY = y;
                        if (Input.Shift)
                        {
                            copyX = Math.Abs(copyX - SelectionSize.X);
                        }
                        if (Input.Alt)
                        {
                            copyY = Math.Abs(copyY - SelectionSize.Y);
                        }
                        Copied[copyX, copyY] = new Tile();
                        Copied[copyX, copyY].type = tile[x + SelectionPosition.X, y + SelectionPosition.Y].type;
                        Copied[copyX, copyY].active = tile[x + SelectionPosition.X, y + SelectionPosition.Y].active;
                        Copied[copyX, copyY].wall = tile[x + SelectionPosition.X, y + SelectionPosition.Y].wall;
                        Copied[copyX, copyY].liquid = tile[x + SelectionPosition.X, y + SelectionPosition.Y].liquid;
                        Copied[copyX, copyY].lava = tile[x + SelectionPosition.X, y + SelectionPosition.Y].lava;
                    }
                }

                Print("Copied Selection");
            }

            if (Input.Ctrl && Input.OldKeyboard.IsKeyUp(Keys.V) && Input.Down(Keys.V) && !editSign)
            {
                if (sel1 != -Vector2.One && sel2 != -Vector2.One)
                {
                    Undo = new Tile[CopiedSize.X, CopiedSize.Y];
                    UndoPosition = new Point((int)sel1.X, (int)sel1.Y);
                    UndoSize = new Point(CopiedSize.X, CopiedSize.Y);
                    for (int x = 0; x < CopiedSize.X; x++)
                    {
                        for (int y = 0; y < CopiedSize.Y; y++)
                        {
                            try
                            {
                                if (Main.tile[x, y] == null)
                                {
                                    Main.tile[x, y] = new Tile();
                                    Undo[x, y] = null;
                                }
                                else
                                {
                                    Undo[x, y] = new Tile();
                                    Undo[x, y].type = Main.tile[x, y].type;
                                    Undo[x, y].liquid = Main.tile[x, y].liquid;
                                    Undo[x, y].lava = Main.tile[x, y].lava;
                                    Undo[x, y].wall = Main.tile[x, y].wall;
                                    Undo[x, y].active = Main.tile[x, y].active;
                                }

                                int copyX = x;
                                int copyY = y;
                                if (Input.Shift)
                                {
                                    copyX = Math.Abs(copyX - CopiedSize.X);
                                }
                                if (Input.Alt)
                                {
                                    copyY = Math.Abs(copyY - CopiedSize.Y);
                                }
                                tile[(int)sel1.X + x, (int)sel1.Y + y] = new Tile();
                                tile[(int)sel1.X + x, (int)sel1.Y + y].type = Copied[copyX, copyY].type;
                                tile[(int)sel1.X + x, (int)sel1.Y + y].active = Copied[copyX, copyY].active;
                                tile[(int)sel1.X + x, (int)sel1.Y + y].wall = Copied[copyX, copyY].wall;
                                tile[(int)sel1.X + x, (int)sel1.Y + y].liquid = Copied[copyX, copyY].liquid;
                                tile[(int)sel1.X + x, (int)sel1.Y + y].lava = Copied[copyX, copyY].lava;
                                TileFrame((int)sel1.X + x, (int)sel1.Y + y);
                                SquareWallFrame((int)sel1.X + x, (int)sel1.Y + y);

                                if (netMode == 1)
                                {
                                    NetMessage.SendData(17, -1, -1, "", 3, x, y, Copied[copyX, copyY].wall, 0);
                                    NetMessage.SendData(17, -1, -1, "", 1, x, y, Copied[copyX, copyY].type, 0);

                                    if (netMode == 1)
                                    {
                                        NetMessage.sendWater(x, y);
                                    }
                                }
                            }
                            catch
                            {

                            }
                        }
                    }
                }

                Print("Pasted Selection");
            }

            #endregion

            #region Erasers

            else if (mouseState.RightButton == ButtonState.Pressed && oldMouseState.RightButton == ButtonState.Released && player[myPlayer].inventory[player[myPlayer].selectedItem].pick >= 1)
            {
                Undo = new Tile[SelectionSize.X, SelectionSize.Y];
                UndoPosition = new Point((int)sel1.X, (int)sel1.Y);
                UndoSize = new Point(SelectionSize.X, SelectionSize.Y);
                for (int xp = 0; xp < SelectionSize.X; xp++)
                {
                    for (int yp = 0; yp < SelectionSize.Y; yp++)
                    {
                        int x = xp + SelectionPosition.X;
                        int y = yp + SelectionPosition.Y;
                        if (SelectedTiles[xp, yp])
                        {
                            if (Main.tile[x, y] == null)
                            {
                                Main.tile[x, y] = new Tile();
                                Undo[xp, yp] = null;
                            }
                            else
                            {
                                Undo[xp, yp] = new Tile();
                                Undo[xp, yp].type = Main.tile[x, y].type;
                                Undo[xp, yp].liquid = Main.tile[x, y].liquid;
                                Undo[xp, yp].lava = Main.tile[x, y].lava;
                                Undo[xp, yp].wall = Main.tile[x, y].wall;
                                Undo[xp, yp].active = Main.tile[x, y].active;
                            }

                            byte wall = Main.tile[x, y].wall;
                            Main.tile[x, y].type = 0;
                            Main.tile[x, y].active = false;
                            Main.tile[x, y].wall = wall;
                            TileFrame(x, y);
                            TileFrame(x, y - 1);
                            TileFrame(x, y + 1);
                            TileFrame(x - 1, y);
                            TileFrame(x + 1, y);
                            SquareWallFrame(x, y, true);

                            if (netMode == 1)
                            {
                                NetMessage.SendData(17, -1, -1, "", 0, x, y, 0, 0);
                            }
                        }
                    }
                }

                if (sel1 != -Vector2.One && sel2 != -Vector2.One)
                    Print("Cleared Selection of Blocks");
            }
            else if (mouseState.RightButton == ButtonState.Pressed && oldMouseState.RightButton == ButtonState.Released && player[myPlayer].inventory[player[myPlayer].selectedItem].hammer >= 1)
            {
                Undo = new Tile[SelectionSize.X, SelectionSize.Y];
                UndoPosition = new Point((int)sel1.X, (int)sel1.Y);
                UndoSize = new Point(SelectionSize.X, SelectionSize.Y);
                for (int xp = 0; xp < SelectionSize.X; xp++)
                {
                    for (int yp = 0; yp < SelectionSize.Y; yp++)
                    {
                        int x = xp + SelectionPosition.X;
                        int y = yp + SelectionPosition.Y;
                        if (SelectedTiles[xp, yp])
                        {
                            if (Main.tile[x, y] == null)
                            {
                                Main.tile[x, y] = new Tile();
                                Undo[xp, yp] = null;
                            }
                            else
                            {
                                Undo[xp, yp] = new Tile();
                                Undo[xp, yp].type = Main.tile[x, y].type;
                                Undo[xp, yp].liquid = Main.tile[x, y].liquid;
                                Undo[xp, yp].lava = Main.tile[x, y].lava;
                                Undo[xp, yp].wall = Main.tile[x, y].wall;
                                Undo[xp, yp].active = Main.tile[x, y].active;
                            }

                            Main.tile[x, y].wall = 0;
                            TileFrame(x, y);
                            TileFrame(x, y - 1);
                            TileFrame(x, y + 1);
                            TileFrame(x - 1, y);
                            TileFrame(x + 1, y);
                            SquareWallFrame(x, y, true);

                            if (netMode == 1)
                            {
                                NetMessage.SendData(17, -1, -1, "", 2, x, y, 0, 0);
                            }
                        }
                    }
                }

                Print("Cleared Selection of Walls");
            }

            #endregion

            #region Liquid (Fill/Erase)

            else if (mouseState.RightButton == ButtonState.Pressed && oldMouseState.RightButton == ButtonState.Released && player[myPlayer].inventory[player[myPlayer].selectedItem].type == 0xcf)
            {
                Undo = new Tile[SelectionSize.X, SelectionSize.Y];
                UndoPosition = new Point((int)sel1.X, (int)sel1.Y);
                UndoSize = new Point(SelectionSize.X, SelectionSize.Y);
                for (int xp = 0; xp < SelectionSize.X; xp++)
                {
                    for (int yp = 0; yp < SelectionSize.Y; yp++)
                    {
                        int x = xp + SelectionPosition.X;
                        int y = yp + SelectionPosition.Y;
                        if (SelectedTiles[xp, yp])
                        {
                            if (Main.tile[x, y] == null)
                            {
                                Main.tile[x, y] = new Tile();
                                Undo[xp, yp] = null;
                            }
                            else
                            {
                                Undo[xp, yp] = new Tile();
                                Undo[xp, yp].type = Main.tile[x, y].type;
                                Undo[xp, yp].liquid = Main.tile[x, y].liquid;
                                Undo[xp, yp].lava = Main.tile[x, y].lava;
                                Undo[xp, yp].wall = Main.tile[x, y].wall;
                                Undo[xp, yp].active = Main.tile[x, y].active;
                            }

                            Main.tile[x, y].liquid = 255;
                            Main.tile[x, y].lava = true;
                            TileFrame(x, y);
                            SquareWallFrame(x, y, true);

                            if (netMode == 1)
                            {
                                NetMessage.sendWater(x, y);
                            }
                        }
                    }
                }

                Print("Filled Selection with Lava");
            }
            else if (mouseState.RightButton == ButtonState.Pressed && oldMouseState.RightButton == ButtonState.Released && player[myPlayer].inventory[player[myPlayer].selectedItem].type == 0xce)
            {
                Undo = new Tile[SelectionSize.X, SelectionSize.Y];
                UndoPosition = new Point((int)sel1.X, (int)sel1.Y);
                UndoSize = new Point(SelectionSize.X, SelectionSize.Y);
                for (int xp = 0; xp < SelectionSize.X; xp++)
                {
                    for (int yp = 0; yp < SelectionSize.Y; yp++)
                    {
                        int x = xp + SelectionPosition.X;
                        int y = yp + SelectionPosition.Y;
                        if (SelectedTiles[xp, yp])
                        {
                            if (Main.tile[x, y] == null)
                            {
                                Main.tile[x, y] = new Tile();
                                Undo[xp, yp] = null;
                            }
                            else
                            {
                                Undo[xp, yp] = new Tile();
                                Undo[xp, yp].type = Main.tile[x, y].type;
                                Undo[xp, yp].liquid = Main.tile[x, y].liquid;
                                Undo[xp, yp].lava = Main.tile[x, y].lava;
                                Undo[xp, yp].wall = Main.tile[x, y].wall;
                                Undo[xp, yp].active = Main.tile[x, y].active;
                            }

                            Main.tile[x, y].liquid = 255;
                            Main.tile[x, y].lava = false;
                            TileFrame(x, y);
                            SquareWallFrame(x, y, true);

                            if (netMode == 1)
                            {
                                NetMessage.sendWater(x, y);
                            }
                        }
                    }
                }

                Print("Filled Selection with Water");
            }
            else if (mouseState.RightButton == ButtonState.Pressed && oldMouseState.RightButton == ButtonState.Released && 
                player[myPlayer].inventory[player[myPlayer].selectedItem].type == 0xcd)
            {
                Undo = new Tile[SelectionSize.X, SelectionSize.Y];
                UndoPosition = new Point((int)sel1.X, (int)sel1.Y);
                UndoSize = new Point(SelectionSize.X, SelectionSize.Y);
                for (int xp = 0; xp < SelectionSize.X; xp++)
                {
                    for (int yp = 0; yp < SelectionSize.Y; yp++)
                    {
                        int x = xp + SelectionPosition.X;
                        int y = yp + SelectionPosition.Y;
                        if (SelectedTiles[xp, yp])
                        {
                            if (Main.tile[x, y] == null)
                            {
                                Main.tile[x, y] = new Tile();
                                Undo[xp, yp] = null;
                            }
                            else
                            {
                                Undo[xp, yp] = new Tile();
                                Undo[xp, yp].type = Main.tile[x, y].type;
                                Undo[xp, yp].liquid = Main.tile[x, y].liquid;
                                Undo[xp, yp].lava = Main.tile[x, y].lava;
                                Undo[xp, yp].wall = Main.tile[x, y].wall;
                                Undo[xp, yp].active = Main.tile[x, y].active;
                            }

                            Main.tile[x, y].liquid = 0;
                            Main.tile[x, y].lava = false;
                            TileFrame(x, y);
                            SquareWallFrame(x, y, true);

                            if (netMode == 1)
                            {
                                NetMessage.sendWater(x, y);
                            }
                        }
                    }
                }

                Print("Drained Selection of Liquid");
            }

            #endregion

            #region Fills

            if (mouseState.RightButton == ButtonState.Pressed && oldMouseState.RightButton == ButtonState.Released && player[myPlayer].inventory[player[myPlayer].selectedItem].createTile >= 0)
            {
                Undo = new Tile[SelectionSize.X, SelectionSize.Y];
                UndoPosition = new Point((int)sel1.X, (int)sel1.Y);
                UndoSize = new Point(SelectionSize.X, SelectionSize.Y);
                for (int xp = 0; xp < SelectionSize.X; xp++)
                {
                    for (int yp = 0; yp < SelectionSize.Y; yp++)
                    {
                        int x = xp + SelectionPosition.X;
                        int y = yp + SelectionPosition.Y;
                        if (SelectedTiles[xp, yp])
                        {
                            if (Main.tile[x, y] == null)
                            {
                                Undo[xp, yp] = null;
                            }
                            else
                            {
                                Undo[xp, yp] = new Tile();
                                Undo[xp, yp].type = Main.tile[x, y].type;
                                Undo[xp, yp].liquid = Main.tile[x, y].liquid;
                                Undo[xp, yp].lava = Main.tile[x, y].lava;
                                Undo[xp, yp].wall = Main.tile[x, y].wall;
                                Undo[xp, yp].active = Main.tile[x, y].active;
                            }

                            byte wall = Main.tile[x, y].wall;
                            Main.tile[x, y] = new Tile();
                            Main.tile[x, y].type = (byte)player[myPlayer].inventory[player[myPlayer].selectedItem].createTile;
                            Main.tile[x, y].wall = wall;
                            Main.tile[x, y].active = true;
                            TileFrame(x, y);
                            SquareWallFrame(x, y, true);

                            if (netMode == 1)
                            {
                                NetMessage.SendData(17, -1, -1, "", 1, x, y, Main.tile[x, y].type, CurrentPlayer.inventory[CurrentPlayer.selectedItem].placeStyle);
                            }
                        }
                    }
                }

                Print("Filled Selection with Block " + player[myPlayer].inventory[player[myPlayer].selectedItem].createTile);
            }
            else if (mouseState.RightButton == ButtonState.Pressed && oldMouseState.RightButton == ButtonState.Released && player[myPlayer].inventory[player[myPlayer].selectedItem].createWall >= 0)
            {
                Undo = new Tile[SelectionSize.X, SelectionSize.Y];
                UndoPosition = new Point((int)sel1.X, (int)sel1.Y);
                UndoSize = new Point(SelectionSize.X, SelectionSize.Y);
                for (int xp = 0; xp < SelectionSize.X; xp++)
                {
                    for (int yp = 0; yp < SelectionSize.Y; yp++)
                    {
                        int x = xp + SelectionPosition.X;
                        int y = yp + SelectionPosition.Y;

                        if (Main.tile[x, y] == null)
                        {
                            Undo[xp, yp] = null;
                        }
                        else
                        {
                            Undo[xp, yp] = new Tile();
                            Undo[xp, yp].type = Main.tile[x, y].type;
                            Undo[xp, yp].liquid = Main.tile[x, y].liquid;
                            Undo[xp, yp].lava = Main.tile[x, y].lava;
                            Undo[xp, yp].wall = Main.tile[x, y].wall;
                            Undo[xp, yp].active = Main.tile[x, y].active;
                        }

                        if (SelectedTiles[xp, yp])
                        {
                            if (Main.tile[x, y] == null)
                            {
                                Main.tile[x, y] = new Tile();
                                Main.tile[x, y].type = 0;
                            }

                            Main.tile[x, y].wall = (byte)player[myPlayer].inventory[player[myPlayer].selectedItem].createWall;
                            TileFrame(x, y);
                            SquareWallFrame(x, y, true);

                            if (netMode == 1)
                            {
                                NetMessage.SendData(17, -1, -1, "", 3, x, y, Main.tile[xp, yp].wall, 0);
                            }
                        }
                    }
                }

                Print("Filled Selection with Wall " + player[myPlayer].inventory[player[myPlayer].selectedItem].createWall);
            }

            #endregion

            #region Undo

            if (Input.Ctrl && Input.OldKeyboard.IsKeyUp(Keys.Z) && Input.Down(Keys.Z) && !editSign)
            {
                for (int xp = 0; xp < UndoSize.X; xp++)
                {
                    for (int yp = 0; yp < UndoSize.Y; yp++)
                    {
                        int x = xp + UndoPosition.X;
                        int y = yp + UndoPosition.Y;

                        if (Undo[xp, yp] == null)
                            tile[x, y] = null;
                        else
                        {
                            tile[x, y] = new Tile();
                            tile[x, y].type = Undo[xp, yp].type;
                            tile[x, y].active = Undo[xp, yp].active;
                            tile[x, y].wall = Undo[xp, yp].wall;
                            tile[x, y].liquid = Undo[xp, yp].liquid;
                            tile[x, y].lava = Undo[xp, yp].lava;
                            TileFrame(x, y);
                            SquareWallFrame(x, y);

                            if (netMode == 1)
                            {
                                NetMessage.SendData(17, -1, -1, "", 3, x, y, Undo[xp, yp].wall, 0);
                                NetMessage.SendData(17, -1, -1, "", 1, x, y, Undo[xp, yp].type, CurrentPlayer.inventory[CurrentPlayer.selectedItem].placeStyle);
                                NetMessage.sendWater(x, y);
                            }
                        }
                    }
                }

                Print("Undo Complete");
            }

            #endregion

            #endregion 
        }

        public void UpdateSelection()
        {
            // Button clicked, set first selection point 
            if (CurrentPlayer.inventory[CurrentPlayer.selectedItem].name != null)
            {
                if (mouseState.LeftButton == ButtonState.Pressed && oldMouseState.LeftButton == ButtonState.Released && player[myPlayer].inventory[player[myPlayer].selectedItem].name.ToLower().Contains("phaseblade"))
                {
                    int x = (int)((Main.mouseState.X + Main.screenPosition.X) / 16f);
                    int y = (int)((Main.mouseState.Y + Main.screenPosition.Y) / 16f);
                    sel1 = new Vector2(x, y);
                }

                // Button is being held down, set second point and make sure the selection points are in the right order
                if (mouseState.LeftButton == ButtonState.Pressed && player[myPlayer].inventory[player[myPlayer].selectedItem].name.ToLower().Contains("phaseblade"))
                {
                    int x = (int)((Main.mouseState.X + Main.screenPosition.X) / 16f);
                    int y = (int)((Main.mouseState.Y + Main.screenPosition.Y) / 16f);
                    sel2 = new Vector2(x, y) + Vector2.One;
                    if (keyState.IsKeyDown(Keys.LeftShift) || keyState.IsKeyDown(Keys.RightShift))
                    {
                        Vector2 size = sel1 - sel2;
                        if (Math.Abs(size.X) != Math.Abs(size.Y))
                        {
                            float min = Math.Min(Math.Abs(size.X), Math.Abs(size.Y));
                            if (sel2.X > sel1.X)
                            {
                                sel2 = new Vector2(sel1.X + min, sel2.Y);
                            }
                            else
                            {
                                sel2 = new Vector2(sel1.X - min, sel2.Y);
                            }
                            if (sel2.Y > sel1.Y)
                            {
                                sel2 = new Vector2(sel2.X, sel1.Y + min);
                            }
                            else
                            {
                                sel2 = new Vector2(sel2.X, sel1.Y - min);
                            }
                        }
                    }
                }

                // Clear selection
                if (mouseState.RightButton == ButtonState.Pressed && player[myPlayer].inventory[player[myPlayer].selectedItem].name.ToLower().Contains("phaseblade"))
                {
                    sel1 = -Vector2.One;
                    sel2 = -Vector2.One;
                }
            }

            // Check inside the selection and set SelectedTiles accordingly
            int minx = (int)Math.Min(sel1.X, sel2.X);
            int maxx = (int)Math.Max(sel1.X, sel2.X);
            int miny = (int)Math.Min(sel1.Y, sel2.Y);
            int maxy = (int)Math.Max(sel1.Y, sel2.Y);
            SelectedTiles = new bool[maxx - minx, maxy - miny];
            SelectionSize = new Point(maxx - minx, maxy - miny);
            SelectionPosition = new Point(minx, miny);
            for (int x = 0; x < SelectionSize.X; x++)
            {
                for (int y = 0; y < SelectionSize.Y; y++)
                {
                    SelectedTiles[x, y] = true;
                }
            }
        }

        public void DrawSelectionOverlay()
        {
            // TODO: Properly cull the tiles so I'm not killing people trying to select massive areas
            // BROKEN: This code offsets the selection position as you move it off the screen to left - i.e, moving right

            if ((sel1 == -Vector2.One && sel2 == -Vector2.One) || (sel1 == Vector2.Zero && sel2 == Vector2.Zero && SelectionSize.X == 0 && SelectionSize.Y == 0))
                return;

            Vector2 offset = new Vector2(((int)(screenPosition.X)), ((int)(screenPosition.Y)));
            int minx = (int)Math.Max(SelectionPosition.X * 16, (SelectionPosition.X * 16) - ((int)(screenPosition.X / 16)) * 16);
            int diffx = (int)(SelectionPosition.X * 16) - minx;
            int maxx = minx + (int)Math.Max(SelectionSize.X * 16, screenWidth) + diffx;
            int miny = (int)Math.Max(SelectionPosition.Y * 16, screenPosition.Y);
            int diffy = (int)(SelectionPosition.Y * 16) - miny;
            int maxy = miny + (int)Math.Min(SelectionSize.Y * 16, screenHeight) + diffy;
            for (int x = minx; x < maxx; x += 16)
            {
                for (int y = miny; y < maxy; y += 16)
                {
                    int tx = (int)((x - minx) / 16);
                    int ty = (int)((y - miny) / 16);
                    if (ty >= SelectionSize.Y)
                        continue;
                    if (tx >= SelectionSize.X)
                        break;
                    if (SelectedTiles[tx, ty])
                    {
                        Vector2 cull = (new Vector2(tx + (minx / 16), ty + (miny / 16)) * 16) - offset;
                        Graphics.SpriteBatch.Draw(Graphics.DefaultTexture, cull, null, SelectionOverlay, 0, Vector2.Zero, 16, SpriteEffects.None, 0);
                    }
                }
            }

            /*Vector2 pos = new Vector2(SelectionPosition.X, SelectionPosition.Y) * 16;
            pos -= CurrentPlayer.position;
            pos += Adrenic.Graphics.Resolution / 2;
            for (int x = 0; x < SelectionSize.X; x++)
            {
                for (int y = 0; y < SelectionSize.Y; y++)
                {
                    if (SelectedTiles[x, y])
                    {
                        Graphics.SpriteBatch.Draw(Graphics.DefaultTexture, pos + new Vector2(8 + (x * 16), 8 + (y * 16)), null, SelectionOverlay, 0, Vector2.Zero, new Vector2(16, 16), SpriteEffects.None, 0);
                    }
                }
            }*/
        }
    }
}
