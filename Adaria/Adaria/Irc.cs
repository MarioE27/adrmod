﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using Microsoft.Xna.Framework;

using Meebey.SmartIrc4net;

using Terraria;

namespace ADRMod
{
    public static class Irc
    {
        public static IrcClient Client = new IrcClient();

        public static void Connect()
        {
            Client.ActiveChannelSyncing = true;

            string name = "";
            string[] names = new string[Main.loadPlayer.Length + 1];
            int i = 0;
            // names[i--] = "adr_testmodule";
            names[Main.loadPlayer.Length] = "adrmod_" + new Random().Next(0, 9999999);
            for (; i < Main.loadPlayer.Length; i++)
            {
                if (Main.loadPlayer[i] != null && Main.loadPlayer[i].name != "")
                {
                    name = names[i] = "adrmod_" + Main.loadPlayer[i].name;
                    break;
                }
                else
                {
                    // names[i--] = "adr_testmodule";
                    names[i] = "adrmod_" + new Random().Next(0, 9999999);
                }
            }

            Client.Connect("irc.adrenic.net", 6667);
            if (name == "")
                Client.Login(names, "ADRMod");
            else
                Client.Login(name, "ADRMod");
            Client.RfcJoin("#adrenic");
            
            Client.AutoRejoin = true;
            Client.AutoRejoinOnKick = true;
            Client.AutoRelogin = true;
            Client.AutoRetry = true;
            Client.AutoReconnect = true;
            
            Client.OnRawMessage += new IrcEventHandler(MessageHandler);
            new Thread(new ThreadStart(Client.Listen)).Start();
        }

        public static void MessageHandler(object sender, IrcEventArgs e)
        {
            string msg = e.Data.Message;
            if (msg == null)
                msg = e.Data.RawMessage;
            if (msg == null)
                return;

            Color c = Color.White;
                                
            if (e.Data.Type == ReceiveType.ChannelAction)
                c = new Color(180, 180, 180);

            if (msg.ToLower().Contains(Client.Nickname.ToLower()))
                c = new Color(60, 180, 255);

            ChatLine cl = new ChatLine();
            cl.color = c;

            if (e.Data.Nick == "" || e.Data.Nick == null)
                cl.text = "!" + msg;
            else
                cl.text = e.Data.Nick + ": " + msg;

            if (e.Data.Type == ReceiveType.ChannelAction)
                cl.text = "*" + e.Data.Nick + " " + msg;
            else if (e.Data.Type == ReceiveType.NickChange)
                cl.text = msg;

            Core.IrcMessages.Add(cl);
        }

        public static void SendMessage(string msg)
        {
            try
            {
                Client.SendMessage(SendType.Message, "#adrenic", msg);
            }
            catch
            {

            }
        }
    }
}
