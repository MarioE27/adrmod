﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace AdrenicLib
{
    public class Button
    {
        public delegate void OnClickHandler(object sender, EventArgs e);

        public string Text = "";
        public bool Visible = false;
        public bool Enabled = false;
        public bool Checked = false;
        public bool Toggles = false;
        public bool Flip = false;
        public SpriteFont Font = Graphics.DefaultFont;
        public Texture2D Texture = Graphics.DefaultTexture;
        public Vector2 Position = Vector2.Zero;
        public Vector2 Offset = Vector2.Zero;
        public Vector2 Size = new Vector2(160, 28);

        bool hover = false;
        bool down = false;

        public event OnClickHandler OnClick;

        public void Update(GameTime gameTime)
        {
            if (!Enabled)
                return;

            bool oldHover = hover;

            Rectangle r = new Rectangle((int)Input.MousePosition.X, (int)Input.MousePosition.Y, 1, 1);
            Rectangle r2 = new Rectangle((int)Position.X + (int)Offset.X, (int)Position.Y + (int)Offset.Y, (int)Size.X, (int)Size.Y);
            hover = r.Intersects(r2);

            if (oldHover == false && hover == true)
            {
                Terraria.Main.soundInstanceMenuTick.Play();
            }

            down = Input.Down(Input.LeftMouse);
            bool clicked = Input.OldMouse.LeftButton == ButtonState.Pressed && !down;

            if (hover && clicked && Visible)
            {
                if (Toggles)
                    Checked = !Checked;

                if (OnClick != null)
                    OnClick.Invoke(this, new EventArgs());

                Terraria.Main.soundInstanceMenuTick.Play();
            }
        }

        public void Draw(GameTime gameTime)
        {
            if (!Visible)
                return;

            int y = 0;
            if (hover)
            {
                y += (int)Size.Y;
            }
            if (down && hover)
                y += (int)Size.Y;

            int x = 0;
            if (Checked)
                x += (int)Size.X;

            Rectangle src = new Rectangle(x, y, (int)Size.X, (int)Size.Y);

            SpriteEffects se = SpriteEffects.None;
            if (Flip)
                se = SpriteEffects.FlipHorizontally;
            Graphics.SpriteBatch.Draw(Texture, Position + Offset, src, Color.White, 0, Vector2.Zero, 1, se, 0);

            Vector2 strSize = Font.MeasureString(Text);
            Vector2 pos = new Vector2(Position.X + 10, Position.Y + (Size.Y / 2) - (strSize.Y / 2) + 1) + Offset;
            if (Flip)
                pos = new Vector2(Position.X + Size.X - 10 - strSize.X + Offset.X, pos.Y);
            Color c = new Color(20, 20, 20);
            Graphics.SpriteBatch.DrawString(Font, Text, pos + new Vector2(1, 1), c);
            Graphics.SpriteBatch.DrawString(Font, Text, pos + new Vector2(1, -1), c);
            Graphics.SpriteBatch.DrawString(Font, Text, pos + new Vector2(-1, 1), c);
            Graphics.SpriteBatch.DrawString(Font, Text, pos + new Vector2(-1, -1), c);
            Graphics.SpriteBatch.DrawString(Font, Text, pos + new Vector2(1, 0), c);
            Graphics.SpriteBatch.DrawString(Font, Text, pos + new Vector2(-1, 0), c);
            Graphics.SpriteBatch.DrawString(Font, Text, pos + new Vector2(0, 1), c);
            Graphics.SpriteBatch.DrawString(Font, Text, pos + new Vector2(0, -1), c);
            Graphics.SpriteBatch.DrawString(Font, Text, pos, Color.White);
        }
    }
}
