﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace AdrenicLib
{
    public class Textarea
    {
        public bool Visible = false;
        public SpriteFont Font = Graphics.DefaultFont;
        public Vector2 Position = Vector2.Zero;
        public Vector2 Offset = Vector2.Zero;
        public Color Color = Color.White;
        public string Text = "";

        public void Draw(int length = -1)
        {
            if (length == -1 || length > Text.Length)
                length = Text.Length;

            Vector2 pos = Position + Offset;
            Color c = new Color(20, 20, 20);
            string text = Text.Substring(0, length);
            Graphics.SpriteBatch.DrawString(Font, text, pos + new Vector2(1, 1), c);
            Graphics.SpriteBatch.DrawString(Font, text, pos + new Vector2(1, -1), c);
            Graphics.SpriteBatch.DrawString(Font, text, pos + new Vector2(-1, 1), c);
            Graphics.SpriteBatch.DrawString(Font, text, pos + new Vector2(-1, -1), c);
            Graphics.SpriteBatch.DrawString(Font, text, pos + new Vector2(1, 0), c);
            Graphics.SpriteBatch.DrawString(Font, text, pos + new Vector2(-1, 0), c);
            Graphics.SpriteBatch.DrawString(Font, text, pos + new Vector2(0, 1), c);
            Graphics.SpriteBatch.DrawString(Font, text, pos + new Vector2(0, -1), c);
            Graphics.SpriteBatch.DrawString(Font, text, pos, Color);
        }
    }

    public class Panel
    {
        public bool Visible = false;
        public bool Enabled = false;
        public bool Resizable = false;
        public bool DrawBackground = true;
        public bool Moveable = true;
        public List<Button> Buttons = new List<Button>();
        public List<Textarea> Textareas = new List<Textarea>();
        public SpriteFont Font = Graphics.DefaultFont;
        public Texture2D Background = Graphics.DefaultTexture;
        public Texture2D TitleBar = Graphics.DefaultTexture;
        public Color BackgroundOverlay = new Color(0x11, 0x11, 0x11, 0x11);
        public Color TitleBarOverlay = Color.White;
        public Color Border = new Color(0x2b, 0x2b, 0x2b);
        public Vector2 Position = Vector2.Zero;
        public Vector2 Size = new Vector2(350, 370);
        public string Text = "";

        bool isDragPosition = true;
        Vector2 originalDownOffset = -(Vector2.One * 5);

        public void Toggle()
        {
            Enabled = (Visible = !Visible);
            AdjustControls();
        }

        public void Show()
        {
            Enabled = Visible = true;
            AdjustControls();
        }

        public void Hide()
        {
            Enabled = Visible = false;
            AdjustControls();
        }

        public void AdjustControls()
        {
            for (int i = 0; i < Buttons.Count; i++)
            {
                Buttons[i].Enabled = Enabled;
                Buttons[i].Visible = Visible;
                Buttons[i].Offset = Position;
            }
            for (int i = 0; i < Textareas.Count; i++)
            {
                Textareas[i].Visible = Visible;
                Textareas[i].Offset = Position;
            }
        }

        public void Update(GameTime gameTime)
        {
            if (Enabled)
            {
                for (int i = 0; i < Buttons.Count; i++)
                {
                    Buttons[i].Update(gameTime);
                }
            }

            bool up = Input.Up(Input.LeftMouse);
            bool clicked = Input.OldMouse.LeftButton == ButtonState.Released && !up;

            Rectangle r = new Rectangle((int)Input.MousePosition.X, (int)Input.MousePosition.Y, 1, 1);
            Rectangle r2 = new Rectangle((int)Position.X, (int)Position.Y, (int)Size.X, 30);
            Rectangle r3 = new Rectangle((int)Position.X + (int)Size.X - 5, (int)Position.Y + (int)Size.Y + TitleBar.Height - 5, 5, 5);
            bool hover1 = r.Intersects(r2);
            bool hover2 = r.Intersects(r3);

            if (hover1 && clicked)
            {
                isDragPosition = true;
                originalDownOffset = Position - Input.MousePosition;
            }
            else if (hover2 && clicked)
            {
                isDragPosition = false;
                originalDownOffset = Position - Input.MousePosition;
            }

            if (originalDownOffset != -(Vector2.One * 5) && !up)
            {
                if (isDragPosition && Moveable)
                    Position += (Input.MousePosition - Input.OldMousePosition);
                else if (Resizable)
                    Size += (Input.MousePosition - Input.OldMousePosition);

                AdjustControls();
            }

            if (Input.Up(Input.LeftMouse))
            {
                originalDownOffset = -(Vector2.One * 5);
            }
        }

        public void Draw(GameTime gameTime)
        {
            if (Visible && Enabled)
            {
                if (DrawBackground)
                {
                    // TitleBar
                    Rectangle r = new Rectangle((int)Position.X, (int)Position.Y, (int)Size.X, TitleBar.Height);
                    Graphics.SpriteBatch.Draw(TitleBar, r, TitleBarOverlay);
                    Graphics.OutlineRectangle(r, Border);

                    // Background
                    r = new Rectangle((int)Position.X, (int)Position.Y + TitleBar.Height, (int)Size.X, (int)Size.Y);
                    Graphics.SpriteBatch.Draw(Background, r, BackgroundOverlay);
                    Graphics.OutlineRectangle(r, Border);
                    
                    Vector2 strSize = Font.MeasureString(Text);
                    float offset = (TitleBar.Height / 2) - (strSize.Y / 2);
                    Vector2 pos = new Vector2(Position.X + offset, Position.Y + offset + 1);
                    Graphics.SpriteBatch.DrawString(Font, Text, pos + new Vector2(1, 1), Color.Black);
                    Graphics.SpriteBatch.DrawString(Font, Text, pos + new Vector2(1, -1), Color.Black);
                    Graphics.SpriteBatch.DrawString(Font, Text, pos + new Vector2(-1, 1), Color.Black);
                    Graphics.SpriteBatch.DrawString(Font, Text, pos + new Vector2(-1, -1), Color.Black);
                    Graphics.SpriteBatch.DrawString(Font, Text, pos + new Vector2(1, 0), Color.Black);
                    Graphics.SpriteBatch.DrawString(Font, Text, pos + new Vector2(-1, 0), Color.Black);
                    Graphics.SpriteBatch.DrawString(Font, Text, pos + new Vector2(0, 1), Color.Black);
                    Graphics.SpriteBatch.DrawString(Font, Text, pos + new Vector2(0, -1), Color.Black);
                    Graphics.SpriteBatch.DrawString(Font, Text, pos, Color.White);
                }

                for (int i = 0; i < Buttons.Count; i++)
                {
                    Buttons[i].Draw(gameTime);
                }
                                
                for (int i = 0; i < Textareas.Count; i++)
                {
                    int charWidth = (int)Textareas[i].Font.MeasureString(Textareas[i].Text).X / Textareas[i].Text.Length;
                    int length = (int)Size.X / charWidth - 1;
                    Textareas[i].Draw(length);
                }
            }
        }
    }
}
