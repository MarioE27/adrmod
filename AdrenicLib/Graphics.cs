﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AdrenicLib
{
    public class Graphics
    {
        public GraphicsDeviceManager GraphicsDeviceManager;
        public GraphicsDevice GraphicsDevice;
        public static Texture2D DefaultTexture;
        public static SpriteFont DefaultFont;
        public static SpriteBatch SpriteBatch;

        public Vector2 Resolution
        {
            get
            {
                return new Vector2(
                    GraphicsDeviceManager.GraphicsDevice.Viewport.Width,
                    GraphicsDeviceManager.GraphicsDevice.Viewport.Height
                    );
            }
        }

        public Graphics(GraphicsDeviceManager gdm)
        {
            GraphicsDeviceManager = gdm;
            GraphicsDevice = gdm.GraphicsDevice;
            SpriteBatch = new SpriteBatch(gdm.GraphicsDevice);
            DefaultTexture = new Texture2D(GraphicsDevice, 1, 1);
            DefaultTexture.SetData<Color>(new Color[] { Color.White });
        }
        
        public static void DrawLine(Vector2 p1, Vector2 p2, Color c)
        {
            Vector2 diff = p1 - p2;
            float a = (float)Math.Atan2(p2.Y - p1.Y, p2.X - p1.X);
            float l = diff.Length();
            Vector2 pos = p1;
            Vector2 scale = new Vector2(l, 1);
            SpriteBatch.Draw(DefaultTexture, pos, null, c, a, Vector2.Zero, scale, SpriteEffects.None, 0);
        }

        public static void OutlineRectangle(Rectangle r, Color c)
        {
            r = new Rectangle(r.X - 1, r.Y - 1, r.Width + 1, r.Height + 1);
            DrawLine(new Vector2(r.X, r.Y), new Vector2(r.X + r.Width, r.Y), c);
            DrawLine(new Vector2(r.X + r.Width, r.Y), new Vector2(r.X + r.Width, r.Y + r.Height), c);
            DrawLine(new Vector2(r.X + r.Width, r.Y + r.Height), new Vector2(r.X, r.Y + r.Height), c);
            DrawLine(new Vector2(r.X, r.Y + r.Height), new Vector2(r.X, r.Y), c);
        }
    }
}
