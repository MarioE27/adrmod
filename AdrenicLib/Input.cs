﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace AdrenicLib
{
    public static class Input
    {
        public static KeyboardState Keyboard = new KeyboardState();
        public static MouseState Mouse = new MouseState();
        public static KeyboardState OldKeyboard = new KeyboardState();
        public static MouseState OldMouse = new MouseState();

        public const int LeftMouse = -1;
        public const int RightMouse = -2;
        public const int MiddleMouse = -3;
        public const int MiscMouse1 = -4;
        public const int MiscMouse2 = -5;

        public static bool Shift = false;
        public static bool Ctrl = false;
        public static bool Alt = false;

        public static void Update(GameTime gameTime)
        {
            OldKeyboard = Keyboard;
            OldMouse = Mouse;
            Keyboard = Microsoft.Xna.Framework.Input.Keyboard.GetState();
            Mouse = Microsoft.Xna.Framework.Input.Mouse.GetState();

            Shift = Down(Keys.LeftShift)    || Down(Keys.RightShift);
            Alt =   Down(Keys.LeftAlt)      || Down(Keys.RightAlt);
            Ctrl =  Down(Keys.LeftControl)  || Down(Keys.RightControl);
        }

        public static bool Pressed(Keys[] keys)
        {
            foreach (Keys k in keys)
            {
                if (Pressed((int)k))
                    return true;
            }

            return false;
        }

        public static bool Pressed(Keys key)
        {
            if (Pressed((int)key))
                return true;

            return false;
        }

        public static bool Pressed(int id)
        {
            switch (id)
            {
                case LeftMouse:
                    return OldMouse.LeftButton == ButtonState.Released && Mouse.LeftButton == ButtonState.Pressed;
                case RightMouse:
                    return OldMouse.RightButton == ButtonState.Released && Mouse.RightButton == ButtonState.Pressed;
                case MiddleMouse:
                    return OldMouse.MiddleButton == ButtonState.Released && Mouse.MiddleButton == ButtonState.Pressed;
                case MiscMouse1:
                    return OldMouse.XButton1 == ButtonState.Released && Mouse.XButton1 == ButtonState.Pressed;
                case MiscMouse2:
                    return OldMouse.XButton2 == ButtonState.Released && Mouse.XButton1 == ButtonState.Pressed;
                default:
                    return (!OldKeyboard.IsKeyDown((Keys)id)) && (Keyboard.IsKeyDown((Keys)id));
            }
        }

        public static bool Pressed(int[] ids)
        {
            foreach (int i in ids)
            {
                if (Pressed(i))
                    return true;
            }
            return false;
        }

        public static bool Down(Keys[] keys)
        {
            foreach (Keys k in keys)
            {
                if (Down((int)k))
                    return true;
            }

            return false;
        }

        public static bool Down(Keys key)
        {
            if (Down((int)key))
                return true;

            return false;
        }

        public static bool Down(int[] ids)
        {
            foreach (int i in ids)
            {
                if (Down(i))
                    return true;
            }
            return false;
        }

        public static bool Down(int id)
        {
            switch (id)
            {
                case LeftMouse:
                    return Mouse.LeftButton == ButtonState.Pressed;
                case RightMouse:
                    return Mouse.RightButton == ButtonState.Pressed;
                case MiddleMouse:
                    return Mouse.MiddleButton == ButtonState.Pressed;
                case MiscMouse1:
                    return Mouse.XButton1 == ButtonState.Pressed;
                case MiscMouse2:
                    return Mouse.XButton2 == ButtonState.Pressed;
                default:
                    return Keyboard.IsKeyDown((Keys)id);
            }
        }

        public static bool Up(Keys[] keys)
        {
            foreach (Keys k in keys)
            {
                if (Up((int)k))
                    return true;
            }

            return false;
        }

        public static bool Up(Keys key)
        {
            if (Up((int)key))
                return true;

            return false;
        }

        public static bool Up(int[] ids)
        {
            foreach (int i in ids)
            {
                if (Up(i))
                    return true;
            }
            return false;
        }

        public static bool Up(int id)
        {
            switch (id)
            {
                case LeftMouse:
                    return Mouse.LeftButton == ButtonState.Released;
                case RightMouse:
                    return Mouse.RightButton == ButtonState.Released;
                case MiddleMouse:
                    return Mouse.MiddleButton == ButtonState.Released;
                case MiscMouse1:
                    return Mouse.XButton1 == ButtonState.Released;
                case MiscMouse2:
                    return Mouse.XButton2 == ButtonState.Released;
                default:
                    return (!Keyboard.IsKeyDown((Keys)id));
            }
        }

        public static Vector2 MousePosition
        {
            get
            {
                return new Vector2(Mouse.X, Mouse.Y);
            }
        }

        public static Vector2 OldMousePosition
        {
            get
            {
                return new Vector2(OldMouse.X, OldMouse.Y);
            }
        }

        public static bool MouseMoved
        {
            get
            {
                return MousePosition != OldMousePosition;
            }
        }
    }
}
