﻿using System;
using System.Collections.Generic;
using System.IO;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

using AdrenicLib;

namespace AdrenicLib
{
    public class Adrenic
    {
        public const string FILE_DATE_TIME_FORMAT = "yyyy-MM-dd_HH-mm-ss";
        
        public static string ScreenshotDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Screenshots\\ADRMod\\";

        public Panel ControlPanel = new Panel();
        public List<Panel> Panels = new List<Panel>();

        public Game Game;
        public Graphics Graphics;
        public string GameName = "Name";
        public string GameVersion = "1.0";

        public Adrenic(Game game, Graphics graphics)
        {
            Game = game;
            Graphics = graphics;
        }

        public string TakeScreenshot(string file = "")
        {
            if (!Directory.Exists(ScreenshotDirectory))
                Directory.CreateDirectory(ScreenshotDirectory);

            if (file == "")
            {
                file = ScreenshotDirectory + "AdrenicLib_Screenshot_" + DateTime.Now.ToString(FILE_DATE_TIME_FORMAT) + ".png";
            }

            /*Texture2D screenshot = new Texture2D(Graphics.GraphicsDevice,
                (int)Graphics.Resolution.X,
                (int)Graphics.Resolution.Y);

            Color[] data = new Color[(int)(Graphics.Resolution.X * Graphics.Resolution.Y)];

            Graphics.GraphicsDevice.GetBackBufferData<Color>(data);

            screenshot.SetData<Color>(data);

            FileStream fs = new FileStream(file, FileMode.Create, FileAccess.Write);
            screenshot.SaveAsPng(fs, (int)Graphics.Resolution.X, (int)Graphics.Resolution.Y);
            fs.Close();*/

            int x = (int)Graphics.Resolution.X;
            int y = (int)Graphics.Resolution.Y;
            int xo = (int)Game.Window.ClientBounds.Left;
            int yo = (int)Game.Window.ClientBounds.Top;
            System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(x, y);
            System.Drawing.Graphics gfx = System.Drawing.Graphics.FromImage((System.Drawing.Image)bmp);
            gfx.CopyFromScreen(xo, yo, 0, 0, new System.Drawing.Size(x, y));
            bmp.Save(file, System.Drawing.Imaging.ImageFormat.Png);

            return file;
        }

        public void Update(GameTime gameTime)
        {
            if (Input.Pressed(Keys.Tab) && Input.Ctrl)
            {
                ControlPanel.Toggle();
            }

            if (ControlPanel.Enabled)
            {

            }

            for (int i = 0; i < Panels.Count; i++)
            {
                Panels[i].Update(gameTime);
            }

            ControlPanel.Update(gameTime);
        }

        public void Draw(GameTime gameTime)
        {
            for (int i = 0; i < Panels.Count; i++)
            {
                Panels[i].Draw(gameTime);
            }

            ControlPanel.Draw(gameTime);
        }

        public void BeginDraw(GameTime gameTime)
        {

        }
    }
}
